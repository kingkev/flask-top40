import os

import requests
import requests_cache

from flask import Flask

from config import TOP40_URL

requests_cache.install_cache(
    cache_name='{}/top40cache'.format(
        os.path.join(os.path.dirname(__file__), 'tmp')), expire_after=43200)


app = Flask(__name__)
app.config.from_object('config')


@app.route('/', methods=['GET'])
def home():
    response = requests.get(TOP40_URL).json()
    data = ''
    for index, item in enumerate(response['entries'], start=1):
        data += '{}. {} - {} \n'.format(
            index,
            item['artist'],
            item['title'])
    return data


if __name__ == '__main__':
    app.run(debug=True)
